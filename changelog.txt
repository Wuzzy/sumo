changelog
---------------


---------------
02-16-2021
---------------
init.lua:
removed deprecated:hub_spawn_point, timemode = 2 -> timemode = "decremental", spawn_cage, rem_cage

added:
changelog,
version control: use /sumo version,


support spectate mode, while not requiring updating to the latest version.